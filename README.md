# Daredevil
The goal of Project Daredevil is to develop a proof of concept implementation of
a distributed automotive system following the EVITA standards, written entirely
in the memory-safe programming language Rust. This project is a part of the course
D0022E — *Project in Computer Science* — given at LTU (Luleå University of Technology).

The system encompasses embedded software interacting with sensors and hardware accelerated cryptographic functions,
applications on Rich OSs (Linux-based), and covers secure communication over both CAN FD and Ethernet connections.

## How it works
In the EVITA specification, in-vehicle modules are placed in three categories:
* **Light modules** - data collection, and interactions with sensors/actuators
* **Medium modules** - intra-vehicle communication between modules
* **Full modules** - Extra-vehicle communication

A light module is then, as this project exemplifies, suitable for reading sensor data and forwarding it to a medium module.

Further information of how EVITA defines what the requirements
are can be read in their [deliverables](https://evita-project.org/deliverables.html).
*WARNING:* the EVITA documentation is very vast and finding exactly what you
are looking for can be difficult.

## Project Structure
The core of the project is divided between two parts which are referred to as modules of different categories:

* [daredevil-small](https://gitlab.com/rust-daredevil-group/daredevil-small) - the light module
* [daredevil-medium](https://gitlab.com/rust-daredevil-group/daredevil-medium) - the medium module

For debugging purposes there is also:

* [daredevil-visualizer](https://gitlab.com/rust-daredevil-group/daredevil-visualizer) -
visualizer for the data output of daredevil-medium.

### [daredevil-small](https://gitlab.com/rust-daredevil-group/daredevil-small)
The **daredevil-small** repository acts as the code base for the light module which purpose is to read sensor distance values and securely communicate these values to the medium module over a CAN FD interface.

Please refer to [the dedicated repository](https://gitlab.com/rust-daredevil-group/daredevil-small) a more detailed account.

### [daredevil-medium](https://gitlab.com/rust-daredevil-group/daredevil-medium)
**daredevil-medium** is as the name implies the repository containing everything regarding the medium module.
In summary, the point of the medium module is to act as a relay of data from the light module to some external entity (a server over an Internet connection in this case).
To this end, the module verifies the integrity of the data received over the CAN adapter, decrypts it, re-encrypts it and then passes that along to the external entity for processing.

In a practical scenario, this module would likely send the sensor data to some central hub in the vehicle that would from the data received perform any necessary actions: warn the driver of an imminent collision, communicate distance between vehicle and object via audio/visual cues, or perhaps even engage the brakes.

This medium module runs in a Linux environment (specifically [PetaLinux](https://www.xilinx.com/products/design-tools/embedded-software/petalinux-sdk.html)) on a Xilinx Zynq UltraScale+™ MPSoC Ultra96 development board.
It can be replaced with a Linux-based laptop with minimal alterations.

Please refer to [the dedicated repository](https://gitlab.com/rust-daredevil-group/daredevil-medium) for more info.

### [daredevil-visualizer](https://gitlab.com/rust-daredevil-group/daredevil-visualizer)
Not a vital part of the project, but a very helpful one for debugging,
the **daredevil-visualizer** repository contains as the name suggests a
terminal-based visualizer for data received from the medium module.

Please refer to [the dedicated repository](https://gitlab.com/rust-daredevil-group/daredevil-visualizer) for more info.

## How to get started
To get the parking sensor system up and running you will need to set it up
according to the following image:
![setup of light + medium module](setup.jpg)

### Setting up the sensors
In our demo we use 4 sensors connected according to the following image:
![sensor chaining](sensorsetup.jpg)
The +5 and BW pin on each sensor should be connected to the 5V pin on the light module. The GND pin on each sensor should be connected to GND on the light module. 
Assign in which order you want the sensors to range (measure distance), looking at the image of the complete system above we range from left to right.
Connect AN from the first sensor to PTC17 on the light module, second sensor to PTC16, third to PTC15, fourth to PTC1.
Connect TX of the first sensor to RX on the second, TX of second to RX of third then TX of third to RX of fourth.
Connect RX of the first sensor to PTC8 on the light module.
Information on where the pins are on the light module can be found on page 5 on [s32k144 quickstart guide](https://www.nxp.com/docs/en/quick-reference-guide/S32K144EVB-QSG.pdf).


<img align="right" style="width: 25%; height: 25%" src="can-pins.png">

### Connecting CAN
The NXP S32K144EVB-Q100 exposes four pins CAN communication.
If viewed from the underside, of four pins three are labeled `CAN_HI`, `CAN_LO`, and `GND` under J13.
Using jumper cables, and the CAN adapter pinout figure to the right, connect as follows:
* `CAN_HI` to `CAN_H`,
* `CAN_LO` to `CAN_L`, and
* `GND` to `GND`.

## Recommended reading
Below is a list of recommended resources to refer to in order to understand this project:

* [The Rust Programming Language](https://doc.rust-lang.org/book/)
* [The Embedded Rust Book](https://rust-embedded.github.io/book/)
* [The Embedonomicon](https://docs.rust-embedded.org/embedonomicon/)
* [KTH — CAN bus](https://www.kth.se/social/upload/526eab8ef2765479ddbd9131/CAN) — an account of the CAN communication protocol
* [CAN Protocol Tutorial](https://www.kvaser.com/can-protocol-tutorial/)
* [Encryption - CBC Mode IV: Secret or Not?](https://defuse.ca/cbcmodeiv.htm)
* [EVITA papers](https://evita-project.org/deliverables.html)

## Authors/Coders
Below is a list of people who have worked on this project (in no particular order, other than year):

### 2019
* Viktor "*tmplt*" Sonesten
* John "*curious*" Elfberg Larsson
* Fredrik Pettersson
* Anton Dahlin
* Björn Paulström
* Mark Håkansson
